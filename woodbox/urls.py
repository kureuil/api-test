"""woodbox URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_nested import routers
from rest_framework.documentation import include_docs_urls
from api import views

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'modules', views.ModuleViewSet)
router.register(r'environments', views.EnvironmentViewSet)
router.register(r'samples', views.SampleViewSet)

modules_router = routers.NestedSimpleRouter(router, r'modules', lookup='module', trailing_slash=False)
modules_router.register(r'environments', views.ModuleEnvironmentsViewSet, base_name='module-environments')

environments_router = routers.NestedSimpleRouter(router, r'environments', lookup='environment', trailing_slash=False)
environments_router.register(r'samples', views.EnvironmentSamplesViewSet, base_name='environment-samples')

urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(modules_router.urls)),
    url(r'^api/v1/', include(environments_router.urls)),
    url('^api/docs/', include_docs_urls(title='Local WoodBox API')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.webapp)
]
