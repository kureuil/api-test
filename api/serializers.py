from .models import Module, Environment, Sample
from rest_framework import serializers


class EnvironmentSerializer(serializers.ModelSerializer):
    module = serializers.PrimaryKeyRelatedField(queryset=Module.objects)

    class Meta:
        model = Environment
        fields = ('id', 'module', 'room', 'start_date', 'threshold_min', 'threshold_max')


class ModuleSerializer(serializers.ModelSerializer):
    environment = EnvironmentSerializer(read_only=True)

    class Meta:
        model = Module
        fields = ('id', 'name', 'type', 'vendor', 'uuid', 'environment')


class SampleSerializer(serializers.ModelSerializer):
    environment = serializers.PrimaryKeyRelatedField(queryset=Environment.objects)

    class Meta:
        model = Sample
        fields = ('id', 'sampled_at', 'value', 'environment')
