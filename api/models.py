from django.db import models


class Module(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    vendor = models.CharField(max_length=255)
    uuid = models.CharField(max_length=255)

    @property
    def environment(self):
        """Returns the active environment of a module instance"""
        environment = Environment.objects.filter(module=self).order_by('-start_date').first()
        return environment

    def __str__(self):
        return 'Module %s (%s)' % (self.name, self.type)


class Environment(models.Model):
    module = models.ForeignKey(Module, related_name='environments', on_delete=models.CASCADE)
    room = models.CharField(max_length=255)
    start_date = models.DateTimeField()
    threshold_min = models.CharField(max_length=100)
    threshold_max = models.CharField(max_length=100)

    def __str__(self):
        return 'Environment %s' % self.room


class Sample(models.Model):
    environment = models.ForeignKey(Environment, related_name='samples', on_delete=models.CASCADE)
    sampled_at = models.DateTimeField(max_length=255)
    value = models.CharField(max_length=255)
