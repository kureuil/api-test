from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.settings import api_settings
from .models import Module, Environment, Sample
from .serializers import ModuleSerializer, EnvironmentSerializer, SampleSerializer


def webapp(request):
    return render(request, 'webapp.html')


class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer


class ModuleEnvironmentsViewSet(viewsets.GenericViewSet):
    serializer_class = EnvironmentSerializer
    queryset = Environment.objects.all()

    def list(self, request, module_pk=None):
        queryset = self.filter_queryset(self.get_queryset().filter(module=module_pk))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, module_pk=None):
        request.data['module'] = module_pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': data[api_settings.URL_FIELD_NAME]}
        except (TypeError, KeyError):
            return {}


class EnvironmentViewSet(viewsets.ModelViewSet):
    queryset = Environment.objects.all()
    serializer_class = EnvironmentSerializer


class EnvironmentSamplesViewSet(viewsets.GenericViewSet):
    queryset = Sample.objects.all()
    serializer_class = SampleSerializer

    def list(self, request, environment_pk=None):
        queryset = self.filter_queryset(self.get_queryset().filter(environment=environment_pk))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, environment_pk=None):
        request.data['environment'] = environment_pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': data[api_settings.URL_FIELD_NAME]}
        except (TypeError, KeyError):
            return {}


class SampleViewSet(viewsets.ModelViewSet):
    queryset = Sample.objects
    serializer_class = SampleSerializer
