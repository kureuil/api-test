from django.contrib import admin
from .models import Module, Environment, Sample


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    pass


@admin.register(Environment)
class EnvironmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Sample)
class SampleAdmin(admin.ModelAdmin):
    pass
